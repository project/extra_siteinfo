Extra Site Information
------------
This module provides drush commands and user interface to see the count of nodes, content types, users & roles existing in the site.

#### Features covered
1. Provides drush commands to get count of nodes, content types, users & roles existing in the site.
2. Provides user interface commands to get count of nodes, content types, users & roles existing in the site

### Installation
* Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

### Usage
* nodetypecount

`drush nodetypecount --help`

`drush nodetypecount`

* nodecount

`drush nodecount --help`

`drush nodecount`

`drush nodecount <content-type-names>`

`drush nodecount page,article`

* rolecount

`drush rolecount --help`

`drush rolecount`

* usercount

`drush usercount --help`

`drush usercount`

`drush usercount <role-ids>`

`drush usercount authenticated,administrator`

 * Navigate to the extra siteinfo page `/admin/reports/extra-siteinfo` for viewing the extra site data.
