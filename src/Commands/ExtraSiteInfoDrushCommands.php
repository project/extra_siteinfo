<?php

namespace Drupal\extra_siteinfo\Commands;

use Drupal\node\Entity\NodeType;
use Drush\Commands\DrushCommands;
use Drush\Log\LogLevel;

/**
 * A Drush command file for the extra siteinfo.
 */
class ExtraSiteInfoDrushCommands extends DrushCommands {

  /**
   * Drush command to get total number of content types in your site.
   *
   * Usage:
   * $ drush nodetypecount --help
   * $ drush nodetypecount
   * $ drush ntc.
   *
   * @command nodetypecount
   * @aliases ntc
   */
  public function nodeTypeCount() {
    try {
      $node_types = NodeType::loadMultiple();
      $this->output()->writeln('Total Number of Content Types : ' . count($node_types));
      foreach ($node_types as $node_type => $value) {
        $this->output()->writeln('Content type name : ' . $node_type);
      }
      return 0;
    }
    catch (\Exception $e) {
      $this->output()->writeln('Exception: ' . $e->getMessage());
      drush_log('Not able to get the total number of content types.', LogLevel::ERROR);
      return 1;
    }
  }

  /**
   * Drush command to get total number of nodes in your site.
   *
   * Usage:
   * $ drush nodecount --help
   * $ drush nodecount <content-type-machine-name>
   * $ drush nodecount page,article.
   *
   * @param string $types
   *   Content type machine names, comma separated values.
   *
   * @command nodecount
   * @aliases nc
   */
  public function nodeCount($types = '') {
    try {
      if (empty($types)) {
        $query = \Drupal::entityQuery('node')->accessCheck(TRUE);
        $count = $query->count()->execute();
        $this->output()->writeln('Total Number of Nodes : ' . $count);
      }
      else {
        $type_array = preg_split("/\,/", $types);
        $node_types = NodeType::loadMultiple();
        foreach ($type_array as $_type) {
          if (array_key_exists($_type, $node_types)) {
            $query = \Drupal::entityQuery('node')->accessCheck(TRUE);
            $query = $query->condition('type', $_type);
            $count = $query->count()->execute();
            $this->output()->writeln("Number of '" . $_type . "' content type nodes : " . $count);
          }
        }
      }
      return 0;
    }
    catch (\Exception $e) {
      $this->output()->writeln('Exception: ' . $e->getMessage());
      drush_log('Not able to get the number of nodes.', LogLevel::ERROR);
      return 1;
    }
  }

  /**
   * Drush command to get number of roles existing in site.
   *
   * Usage:
   * $ drush rolecount --help
   * $ drush rolecount
   * $ drush rc.
   *
   * @command rolecount
   * @aliases rc
   */
  public function roleCount() {
    try {
      $roles = user_roles();
      $this->output()->writeln('Total Number of Roles : ' . count($roles));
      foreach ($roles as $_role => $value) {
        $this->output()->writeln('Role name : ' . $_role);
      }
      return 0;
    }
    catch (\Exception $e) {
      $this->output()->writeln('Exception: ' . $e->getMessage());
      drush_log('Not able to get the total number of roles.', LogLevel::ERROR);
      return 1;
    }
  }

  /**
   * Drush command to get user count based on their role.
   *
   * Usage:
   * $ drush usercount --help
   * $ drush usercount
   * $ drush usercount <role_ids>
   * $ drush usercount authenticated,administrator.
   *
   * @param string $roles
   *   Role machine names, comma separated values.
   *
   * @command usercount
   * @aliases uc
   */
  public function userCount($roles = '') {
    try {
      $helper = \Drupal::service('extra_siteinfo.helper');
      if (empty($roles)) {
        $this->output()->writeln('Total Number of Users : ' . $helper->numberOfUsers());
      }
      else {
        $roles_array = preg_split("/\,/", $roles);
        $roles = user_roles();
        foreach ($roles_array as $_role) {
          if (array_key_exists($_role, $roles)) {
            $this->output()->writeln("Number of users with role id '" . $_role . "' are : " . $helper->numberOfUsersByQuery('roles', $_role) . " users.");
          }
        }
      }
      return 0;
    }
    catch (\Exception $e) {
      $this->output()->writeln('Exception: ' . $e->getMessage());
      drush_log('Not able to get the number of users.', LogLevel::ERROR);
      return 1;
    }
  }

}
