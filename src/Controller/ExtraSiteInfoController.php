<?php

namespace Drupal\extra_siteinfo\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Extra Siteinfo controller to show the data.
 */
class ExtraSiteInfoController extends ControllerBase {

  /**
   * Callback function to render the extra siteinfo.
   */
  public function extraSiteinfoPage() {
    $output = [];
    $helper = \Drupal::service('extra_siteinfo.helper');
    $output[] = [
      '#theme' => 'extra_siteinfo_page',
      '#desc' => $this->t('This Extra Site Information contains count of nodes, content types, users & roles existing in the site.'),
      '#number_of_roles' => $helper->numberOfRoles(),
      '#number_of_users' => $helper->numberOfUsers(),
      '#number_of_active_users' => $helper->numberOfUsersByQuery('status', 1),
      '#number_of_blocked_users' => $helper->numberOfUsersByQuery('status', 0),
      '#number_of_users_by_role' => $helper->numberOfUsersByRoles(),
      '#number_of_types' => $helper->numberOfTypes(),
      '#number_of_nodes' => $helper->numberOfNodes(),
      '#number_of_published_nodes' => $helper->numberOfNodesByQuery('status', 1),
      '#number_of_unpublished_nodes' => $helper->numberOfNodesByQuery('status', 0),
      '#number_of_nodes_by_types' => $helper->numberOfNodesByTypes(),
    ];
    return $output;
  }

  /**
   * Callback function to render the extra siteinfo.
   */
//   public function extraSiteinfoPageAdvanced() {
//     $output = [];
//     $helper = \Drupal::service('extra_siteinfo.helper');
//     $output[] = [
//       '#theme' => 'extra_siteinfo_page_advanced',
//       '#data' => $this->getAdvancedExtraSiteInfo(),
//       '#content_type_total' => $helper->getContentTypeCount(),
//       '#content_type_names' => $helper->getContentTypeNames(),
//       '#field_storage_name_count' => $helper->getFieldStorageNamesCountByType('node'),
//       '#field_storage_names' => $helper->getFieldStorageNamesByType('node'),
//       '#field_defination_name_count' => $helper->getFieldDefinitionNamesCountByType('node', 'article'),
//       '#field_defination_names' => $helper->getFieldDefinitionNamesByType('node', 'api_docs'),
//     ];
//     return $output;
//   }

//   protected function getAdvancedExtraSiteInfo() {
//     $data = [];
//     $helper = \Drupal::service('extra_siteinfo.helper');
//     $types = ['node'];
//     foreach ($types as $type) {
//       if ($type = 'node') {
//         $data[$type]['count'] = $helper->getContentTypeCount();
//         $data[$type]['count_field_storages'] = $helper->getFieldStorageNamesCountByType($type);
//         $data[$type]['field_storages'] = $helper->getFieldStorageNamesCountByType($type);
//         foreach ($helper->getContentTypeNames() as $bundle) {
//           $data[$type][$_type]['count_field_definitions'] = $helper->getFieldDefinitionNamesCountByType($type, $bundle);
//           $data[$type][$_type]['field_definitions'] = $helper->getFieldDefinitionNamesByType($type, $bundle);
//         }
//       }
//     }
//     return $data;
//   }

}
