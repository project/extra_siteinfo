<?php

namespace Drupal\extra_siteinfo;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;

/**
 * A Drupal service with business logic for extra siteinfo.
 */
class ExtraSiteInfoHelper {
  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a RegistrationAccessCheck object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Creates a query to get users.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   A query interface.
   */
  protected function userQuery(): QueryInterface {
    return $this->entityTypeManager->getStorage('user')->getQuery();
  }

  /**
   * Creates a query to get nodes.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   A query interface.
   */
  protected function nodeQuery(): QueryInterface {
    return $this->entityTypeManager->getStorage('node')->getQuery();
  }

  /**
   * Creates a query to get node types.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   A query interface.
   */
  protected function nodeTypeQuery(): QueryInterface {
    return $this->entityTypeManager->getStorage('node_type')->getQuery();
  }
  protected function blockTypeQuery(): QueryInterface {
    return $this->entityTypeManager->getStorage('block_content_type')->getQuery();
  }
  protected function typeQuery($type): QueryInterface {
    return $this->entityTypeManager->getStorage($type)->getQuery();
  }

  /**
   * Creates a entity manager to get field storages.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   *   A entity field manager interface.
   */
  protected function entityFieldStorage($entity_id): array {
    return $this->entityFieldManager->getFieldStorageDefinitions($entity_id);
  }

  /**
   * Creates a entity manager to get field definitions.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   *   A entity field manager interface.
   */
  protected function entityFieldDefinitions($entity_id, $bundle): array {
    return $this->entityFieldManager->getFieldDefinitions($entity_id, $bundle);
  }

  /**
   * Uses the user query to get the number of users.
   */
  public function numberOfUsers(): int {
    return $this->userQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }

  /**
   * Uses the user query to get the number of users by query.
   */
  public function numberOfUsersByQuery($key, $value): int {
    return $this->userQuery()
      ->condition($key, $value)
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }
  public function numberOfUsersByQueryStatus($key, $value, $status): int {
    return $this->userQuery()
      ->condition($key, $value)
      ->condition('status', $status)
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }

  /**
   * Query to get the number of roles.
   */
  public function numberOfRoles(): int {
    return count(user_roles());
  }

  /**
   * Query to get the number of users by roles.
   */
  public function numberOfUsersByRoles() {
    $users_by_role = [];
    $roles = user_roles();
    foreach ($roles as $role => $value) {
      $users_by_role[$role]['total'] = $this->numberOfUsersByQuery('roles', $role);
      $users_by_role[$role]['active'] = $this->numberOfUsersByQueryStatus('roles', $role, '1');
      $users_by_role[$role]['blocked'] = $this->numberOfUsersByQueryStatus('roles', $role, '0');
    }
    return $users_by_role;
  }

  /**
   * Uses the node query to get the number of nodes.
   */
  public function numberOfNodes(): int {
    return $this->nodeQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }

  /**
   * Uses the node query to get the number of nodes by query.
   */
  public function numberOfNodesByQuery($key, $value): int {
    return $this->nodeQuery()
      ->condition($key, $value)
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }
  public function numberOfNodesByQueryStatus($key, $value, $status): int {
    return $this->nodeQuery()
      ->condition($key, $value)
      ->condition('status', $status)
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }

  /**
   * Query to get the number of roles.
   */
  public function numberOfTypes(): int {
    return count(NodeType::loadMultiple());
  }

  /**
   * Query to get the number of nodes by content types.
   */
  public function numberOfNodesByTypes() {
    $nodes_by_types = [];
    $types = NodeType::loadMultiple();
    foreach ($types as $type => $value) {
      $nodes_by_types[$type]['total'] = $this->numberOfNodesByQuery('type', $type);
      $nodes_by_types[$type]['published'] = $this->numberOfNodesByQueryStatus('type', $type, '1');
      $nodes_by_types[$type]['unpublished'] = $this->numberOfNodesByQueryStatus('type', $type, '0');
    }
    return $nodes_by_types;
  }

  public function getContentTypeCount(): int {
    return $this->nodeTypeQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }
  public function getBlockTypeCount(): int {
    return $this->blockTypeQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }
  public function getTypeCount($type): int {
    return $this->typeQuery($type)
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }

  public function getContentTypeNames(): array {
    return $this->nodeTypeQuery()->execute();
  }
  public function getBlockTypeNames(): array {
    return $this->blockTypeQuery()->execute();
  }
  public function getTypeNames($type): array {
    return $this->typeQuery($type)
      ->accessCheck(FALSE)
      ->execute();
  }

  public function getFieldStorageNamesCountByType($type): int {
    return count($this->entityFieldStorage($type));
  }

  public function getFieldStorageNamesByType($type): array {
    return array_keys($this->entityFieldStorage($type));
  }

  public function getFieldDefinitionNamesCountByType($type, $bundle): int {
    return count($this->entityFieldDefinitions($type, $bundle));
  }

  public function getFieldDefinitionNamesByType($type, $bundle): array {
    return array_keys($this->entityFieldDefinitions($type, $bundle));
  }

  public function getFieldDefinitionByType($type, $bundle): array {
    return $this->entityFieldDefinitions($type, $bundle);
  }

}
