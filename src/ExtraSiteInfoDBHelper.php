<?php

namespace Drupal\extra_siteinfo;

use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Database\Connection;

/**
 * A Drupal service with database logics for extra siteinfo.
 */
class ExtraSiteInfoDBHelper {
  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a RegistrationAccessCheck object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   A Database connection to use for reading and writing database data.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  public function getAllTables() {
    $tables = ['Select Table'];
    $data = $this->database
      ->query("SHOW TABLES")
      ->fetchAll();
    foreach ($data as $key => $value) {
      $tables[$value->Tables_in_db] = $value->Tables_in_db;
    }
    return $tables;
  }
  public function getTableDescribe($table) {
    return $this->database
        ->query("DESC $table")
        ->fetchAll();
  }
  public function getTableDataCount($table) {
    $table_data_count = $this->database
        ->query("SELECT COUNT(*) FROM $table")
        ->fetchAll();
    $table_data_count = (array) $table_data_count[0];
    return $table_data_count["COUNT(*)"];
  }
  public function getTableData($table) {
    return $this->database
        ->query("SELECT * FROM $table LIMIT 5")
        ->fetchAll();
  }

  public function getTableDataBySQL($sql) {
//   echo $sql; die;
    return $this->database
        ->query($sql)
        ->fetchAll();
  }

}
