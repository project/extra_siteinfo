<?php

namespace Drupal\extra_siteinfo\Form;

use Drupal\extra_siteinfo\ExtraSiteInfoDBHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExtraSiteInfoDatabaseForm extends FormBase {

  /**
   * DB helper
   *
   * @var \Drupal\extra_siteinfo\ExtraSiteInfoDBHelper.
   */
  protected $db_helper;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new Get User Session.
   *
   * @param \Drupal\extra_siteinfo\ExtraSiteInfoDBHelper
   *   A Database connection to use for reading database data.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(ExtraSiteInfoDBHelper $db_helper, StateInterface $state) {
    $this->db_helper = $db_helper;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extra_siteinfo.db_helper'),
      $container->get('state'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'extra_siteinfo_page_database';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $table = $this->state->get('table');
    $form['extra_siteinfo_db'] = [
      '#type' => 'fieldset',
      '#description' => 'Clear the form for every query for accurate results.',
      '#description_display' => 'before',
      '#title' => t('Search'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $tables = $this->db_helper->getAllTables();
    $form['extra_siteinfo_db']['tables_count'] = [
      '#type' => 'item',
      '#title' => $this->t('Tables Count'),
      '#markup' => count($tables),
    ];
    $form['extra_siteinfo_db']['table'] = [
      '#type' => 'select',
      '#title' => $this->t('Tables'),
      '#options' => $tables,
      '#default_value' => $table
    ];
    if (!empty($table)) {
      $form = $this->renderTableDescribe($form, $table);
      $renderTableCount = $this->renderTableCount($form, $table);
      $form = $renderTableCount[0];
      if ($renderTableCount['table_data_count']) {
        $renderTableData = $this->renderTableData($form, $table);
        $form = $renderTableData['form'];
        $query_options = [
          'where' => 'where'
        ];
        $query = $this->state->get('query');
        $form['extra_siteinfo_db']['query'] = [
          '#type' => 'select',
          '#title' => $this->t('Query with field?'),
          '#options' => $this->addSelectToOptions($this->arrayKeysReplaceWithValues($renderTableData['header'])),
          '#default_value' => $query
        ];
        $query_value = $this->state->get('query_value');
        $form['extra_siteinfo_db']['query_value'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Query value?'),
          '#default_value' => $query_value
        ];
        if (!empty($query)) {
          $form = $this->renderTableQueryCount($form, $table, $query);
          $form = $this->renderTableQuery($form, $table, $query);
          if (!empty($query_value)) {
            $form = $this->renderTableQueryValueCount($form, $table, $query, $query_value);
            $form = $this->renderTableQueryValue($form, $table, $query, $query_value);
          }
        }
      }
    }
    $form['extra_siteinfo_db']['search_submit'] = [
      '#type' => 'submit',
      '#value' => 'Search',
    ];
    $form['extra_siteinfo_db']['clear_submit'] = [
      '#type' => 'submit',
      '#value' => 'Clear',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {}
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operation = $form_state->getValue('op');
    if ($operation == 'Search') {
      $this->state->set('table', $form_state->getValue('table'));
      $this->state->set('query', $form_state->getValue('query'));
      $this->state->set('query_value', $form_state->getValue('query_value'));
    }
    if ($operation == 'Clear') {
      $this->state->delete('table');
      $this->state->delete('query');
      $this->state->delete('query_value');
      $form_state->setRedirect('extra_siteinfo.page_database');
    }
  }

  protected function renderTableDescribe($form, $table) {
    $form['report_describe'] = [
      '#type' => 'fieldset',
      '#title' => t("$ describe " . $table . ";"),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $table_describe = $this->db_helper->getTableDescribe($table);
    $table_describe_header = array_keys((array) $table_describe[0]);
    $rows = $this->getTableRows($table_describe, $table_describe_header);
    $form['report_describe']['count'] = [
      '#type' => 'markup',
      '#markup' => "Number of fieds in the table '" . $table . "': <b>" . count($rows) . "</b>",
    ];
    $form['report_describe']['describe'] = [
      '#type' => 'table',
      '#header' => $table_describe_header,
      '#rows' => $rows,
    ];
    return $form;
  }
  protected function renderTableCount($form, $table) {
    $form['report_count'] = [
      '#type' => 'fieldset',
      '#title' => t("$ select count(*) from " . $table . ";"),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $table_data_count = $this->db_helper->getTableDataCount($table);
    $form['report_count']['count'] = [
      '#type' => 'markup',
      '#markup' => $table_data_count,
    ];
    return [$form, 'table_data_count' => $table_data_count];
  }
  protected function renderTableData($form, $table) {
    $form['report_data'] = [
      '#type' => 'fieldset',
      '#title' => t("$ select * from " . $table . " limit 5;"),
      '#description' => 'Run the query without limit to get full result.',
      '#description_display' => 'before',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $table_data = $this->db_helper->getTableData($table);
    $table_data_header = array_keys((array) $table_data[0]);
    $rows = $this->getTableRows($table_data, $table_data_header);
    $form['report_data']['data'] = [
      '#type' => 'table',
      '#header' => $table_data_header,
      '#rows' => $rows,
    ];
    return ['form' => $form, 'header' => $table_data_header];
  }
  protected function renderTableQueryCount($form, $table, $query) {
    $form['report_data_query_count'.$table] = [
      '#type' => 'fieldset',
      '#title' => t("$ select count(distinct ". $query .")  from " . $table . ";"),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $sql = "select count(distinct ". $query .")  from " . $table . ";";
    $table_data = $this->db_helper->getTableDataBySQL($sql);
    $table_data_header = array_keys((array) $table_data[0]);
    $rows = $this->getTableRows($table_data, $table_data_header);
    $form['report_data_query_count'.$table]['data'] = [
      '#type' => 'table',
      '#header' => $table_data_header,
      '#rows' => $rows,
    ];
    return $form;
  }
  protected function renderTableQuery($form, $table, $query) {
    $form['report_data_query'.$table] = [
      '#type' => 'fieldset',
      '#title' => t("$ select distinct ". $query ."  from " . $table . " limit 5;"),
      '#description' => 'Run the query without limit to get full result.',
      '#description_display' => 'before',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $sql = "select distinct ". $query ."  from " . $table . " limit 5;";
    $table_data = $this->db_helper->getTableDataBySQL($sql);
    $table_data_header = array_keys((array) $table_data[0]);
    $rows = $this->getTableRows($table_data, $table_data_header);
    $form['report_data_query'.$table]['data'] = [
      '#type' => 'table',
      '#header' => $table_data_header,
      '#rows' => $rows,
    ];
    return $form;
  }
  protected function renderTableQueryValueCount($form, $table, $query, $query_value) {
    $form['report_data_query_value_count'.$table] = [
      '#type' => 'fieldset',
      '#title' => t("$ select count(*)  from " . $table . " where ". $query ." = '". $query_value ."';"),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $sql = "select count(*)  from " . $table . " where ". $query ." = '". $query_value ."';";
    $table_data = $this->db_helper->getTableDataBySQL($sql);
    $table_data_header = array_keys((array) $table_data[0]);
    $rows = $this->getTableRows($table_data, $table_data_header);
    $form['report_data_query_value_count'.$table]['data'] = [
      '#type' => 'table',
      '#header' => $table_data_header,
      '#rows' => $rows,
    ];
    return $form;
  }
  protected function renderTableQueryValue($form, $table, $query, $query_value) {
    $form['report_data_query_value'.$table] = [
      '#type' => 'fieldset',
      '#title' => t("$ select *  from " . $table . " where ". $query ." = '". $query_value ."' limit 5;"),
      '#description' => 'Run the query without limit to get full result.',
      '#description_display' => 'before',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $sql = "select *  from " . $table . " where ". $query ." = '". $query_value ."' limit 5;";
    $table_data = $this->db_helper->getTableDataBySQL($sql);
    $table_data_header = array_keys((array) $table_data[0]);
    $rows = $this->getTableRows($table_data, $table_data_header);
    $form['report_data_query_value'.$table]['data'] = [
      '#type' => 'table',
      '#header' => $table_data_header,
      '#rows' => $rows,
    ];
    return $form;
  }

  protected function getTableRows($data, $header) {
    $rows = [];
    foreach ($data as $key => $value) {
      $value = (array) $value;
//       if ($value['Type'] == 'longblob') continue;
      foreach ($header as $_key => $_value) {
//         if ($_value == 'data') continue;
        $rows[$key][$_value] = $value[$_value];
      }
    }
    return $rows;
  }
  protected function arrayKeysReplaceWithValues($array) {
    $_array = [];
    foreach ($array as $key => $value) {
      $_array[$value] = $value;
    }
    return $_array;
  }
  protected function addSelectToOptions($array) {
    array_unshift($array, "Select");
    return $array;
  }

}
