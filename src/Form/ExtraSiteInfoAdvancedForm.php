<?php

namespace Drupal\extra_siteinfo\Form;

use Drupal\extra_siteinfo\ExtraSiteInfoDBHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExtraSiteInfoAdvancedForm extends FormBase {

  /**
   * DB helper
   *
   * @var \Drupal\extra_siteinfo\ExtraSiteInfoDBHelper.
   */
  protected $db_helper;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new Get User Session.
   *
   * @param \Drupal\extra_siteinfo\ExtraSiteInfoDBHelper
   *   A Database connection to use for reading database data.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(ExtraSiteInfoDBHelper $db_helper, StateInterface $state) {
    $this->db_helper = $db_helper;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('extra_siteinfo.db_helper'),
      $container->get('state'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'extra_siteinfo_page_advanced_';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $search = $this->state->get('search');
    $form['extra_siteinfo_advanced'] = [
      '#type' => 'fieldset',
      '#description' => '',
      '#description_display' => 'before',
      '#title' => t('Search'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $options = [
      '' => 'Select',
      'node' => 'Content Type',
      'block' => 'Block Type',
//       'user' => 'User',
//       'term' => 'Vocabulary',
    ];
    $form['extra_siteinfo_advanced']['search'] = [
      '#type' => 'select',
      '#title' => $this->t('Search'),
      '#options' => $options,
      '#default_value' => $search
    ];
    $form['extra_siteinfo_advanced']['search_submit'] = [
      '#type' => 'submit',
      '#value' => 'Search',
    ];
    $form['extra_siteinfo_advanced']['clear_submit'] = [
      '#type' => 'submit',
      '#value' => 'Clear',
    ];
    if (!empty($search)) {
      $form['extra_siteinfo_advanced']['report_search'] = [
        '#type' => 'fieldset',
//         '#title' => t("Field Storages & Field Definations Report for " . $options[$search]),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      ];
      $data = [];
      $helper = \Drupal::service('extra_siteinfo.helper');
      if ($search == "node") {
        $form = $this->getAdvancedExtraSiteInfoOfNode($form, $helper, $search);
      }
      if ($search == "block") {
        $form = $this->getAdvancedExtraSiteInfoOfBlock($form, $helper, $search);
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {}
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operation = $form_state->getValue('op');
    if ($operation == 'Search') {
      $this->state->set('search', $form_state->getValue('search'));
    }
    if ($operation == 'Clear') {
      $this->state->delete('search');
      $form_state->setRedirect('extra_siteinfo.page_advanced');
    }
  }

  protected function getAdvancedExtraSiteInfoOfNode($form, $helper, $search) {
    $form['extra_siteinfo_advanced']['report_search']['1'] = [
      '#type' => 'table',
      '#header' => ['Total number of Content Types', 'Total Field Storages in all Content Types'],
      '#rows' => [[$helper->getContentTypeCount(), $helper->getFieldStorageNamesCountByType($search)]],
      '#caption' => $this->t('Below is the report of the all the field storages in all the content types'),
    ];
    $all_field_storages = [];
    foreach ($helper->getFieldStorageNamesByType($search) as $_value) {
      $all_field_storages[] = [$_value];
    }
    $form['extra_siteinfo_advanced']['report_search']['2'] = [
      '#type' => 'table',
      '#header' => ['Field Storages in all Content Types'],
      '#rows' => $all_field_storages,
      '#caption' => $this->t('Below is the report of the all the field storages in all the content types'),
    ];
    $types = $helper->getContentTypeNames($search);
    $nodes_by_types = [];
    foreach ($types as $key => $bundle) {
      $nodes_by_types[$bundle]['type'] = $bundle;
      $nodes_by_types[$bundle]['total'] = $helper->numberOfNodesByQuery('type', $bundle);
      $nodes_by_types[$bundle]['published'] = $helper->numberOfNodesByQueryStatus('type', $bundle, '1');
      $nodes_by_types[$bundle]['unpublished'] = $helper->numberOfNodesByQueryStatus('type', $bundle, '0');
    }
    $form['extra_siteinfo_advanced']['report_search'][$key] = [
      '#type' => 'table',
      '#header' => ['Content Type', 'Number of Nodes', 'Number of Published Nodes', 'Number of Unpublished Nodes'],
      '#rows' => $nodes_by_types,
      '#caption' => $this->t('Below is the report of content associated with all the content types'),
    ];
    foreach ($types as $key => $bundle) {
      $form['extra_siteinfo_advanced']['report_search']['_'.$key] = [
        '#type' => 'table',
        '#header' => ['Total Field Definitions in ' . $bundle],
        '#rows' => [[$helper->getFieldDefinitionNamesCountByType($search, $bundle)]],
        '#caption' => $this->t('Below is the report of the all the field definitions of the <b>"'.$bundle.'"</b> the content type'),
      ];
      $field_definition = [];
      $field_required = [];
      foreach ($helper->getFieldDefinitionByType($search, $bundle) as $_value) {
        $field_definition[] = [
          $_value->getLabel(),
          $_value->getName(),
          $_value->getType(),
          $_value->getDescription(),
          $_value->isRequired(),
          $_value->isTranslatable(),
          $_value->getTargetBundle(),
        ];
      }
      $form['extra_siteinfo_advanced']['report_search']['__'.$key] = [
        '#type' => 'table',
        '#header' => [
          'Field Name',
          'Field Machine Name',
          'Field Type',
          'Field Description',
          'Required?',
          'Translatable?',
          'Content Type',
        ],
        '#rows' => $field_definition,
        '#caption' => $this->t('Below is the report of the all the field definitions of the <b>"'.$bundle.'"</b> the content type'),
      ];
    }
    return $form;
  }

  protected function getAdvancedExtraSiteInfoOfBlock($form, $helper, $search) {
    $form['extra_siteinfo_advanced']['report_search']['1'] = [
      '#type' => 'table',
      '#header' => ['Total number of Block Types', 'Total Field Storages in all Content Types'],
      '#rows' => [[$helper->getBlockTypeCount()]],
      '#caption' => $this->t('Below is the report of the all the field storages in all the content types'),
    ];
    return $form;
  }

}
